;
function createNewUser() {
    const FIRST_NAME = prompt('Enter your name', 'Ivan');
    const LAST_NAME = prompt('Enter your surname', 'Ivanov');
    const USER_BIRTH = prompt('Please enter your birth date in format dd.mm.yyyy', '01.01.1970');
    const NEW_USER = {
        name: FIRST_NAME,
        surname: LAST_NAME,
        getAge: (function() {
                    let now = new Date();
                    let monthOfBirth = +USER_BIRTH.slice(4, 6) - 1;
                    if (now.getFullYear() > +USER_BIRTH.slice(6, 10)){
                        if (now.getMonth() >= monthOfBirth) {
                            if (now.getDate() >= +USER_BIRTH.slice(0, 2)) {
                                return (now.getFullYear() - +USER_BIRTH.slice(6, 10));
                            } else {
                                return (now.getFullYear() - (+USER_BIRTH.slice(6, 10) + 1));
                            }
                        } else {
                            return (now.getFullYear() - (+USER_BIRTH.slice(6, 10) + 1));
                        }
                    } else {
                        userBirth = prompt('Please enter your birth date in format dd.mm.yyyy', 'Example - 01.01.1970');
                    }
                })(),
        login: FIRST_NAME.slice(0, 1).toLowerCase() + LAST_NAME.toLowerCase(),
        password: FIRST_NAME.slice(0, 1).toUpperCase() + LAST_NAME.toLowerCase() + USER_BIRTH.slice(6, 10),
    }
    console.log('Your age is ' + NEW_USER.getAge);
    console.log('Your generated password is ' + NEW_USER.password);
    return NEW_USER;
}

console.log(createNewUser());

