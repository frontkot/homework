;
function createNewUser() {
    const FIRST_NAME = prompt('Enter your name', 'Ivan');
    const LAST_NAME = prompt('Enter your surname', 'Ivanov');
    const NEW_USER = {
        name: FIRST_NAME,
        surname: LAST_NAME,
        login: FIRST_NAME.slice(0, 1).toLowerCase() + LAST_NAME.toLowerCase()
    }
    return NEW_USER;
}

console.log(createNewUser());

