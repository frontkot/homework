const array = ['value', () => 'showValue'];

const value = array[0];
showValue = () => array[1](); 

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'
