const user1 = {
    name: "John",
    years: 30
};

function displayUser (user1) {
    const {name, years: age, isAdmin = false} = user1;
    const container = document.createElement('div');
    this.name = document.createElement('p');
    this.age = document.createElement('p');
    this.isAdmin = document.createElement('p');

    
    this.name.innerText = `Name - ${name}`;
    this.age.innerText = `Age - ${age}`;
    this.isAdmin.innerText = `Is admin - ${isAdmin}`;

    document.body.prepend(container);
    document.querySelector('div').prepend(this.name, this.age, this.isAdmin);
};

const newUser = new displayUser(user1);