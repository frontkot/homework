const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}
  


function displayUser (employee) {
    const mayor = {...employee, age: 49, salary: 20387};
    const {name, surname, age, salary} = mayor;
    const container = document.createElement('div');
    this.name = document.createElement('p');
    this.surname = document.createElement('p');
    this.age = document.createElement('p');
    this.salary = document.createElement('p');

    
    this.name.innerText = `Name - ${name}`;
    this.surname.innerText = `Surname - ${surname}`;
    this.age.innerText = `Age - ${age} years`;
    this.salary.innerText = `Salary - ${salary} uah`;

    document.body.prepend(container);
    document.querySelector('div').prepend(this.name, this.surname, this.age, this.salary);
};

const newEmployee = new displayUser(employee);